#include<stdio.h>
#include<string.h>

struct
{
    char str[99]; //单词
    
    int n;  //出现次数
    
}a[99]={"",0},t; //初始化

int *getCharNum(char *name,int *totalNum);

int main(){
	
	char name[100];

	int totalNum[3]={0,0,0};
	 
	
	
	printf("输入所需查询文件名及其路径: ");
	 
	scanf("%s", &name);
							
	if(getCharNum(name, totalNum)){
		
		printf("characters: %d\nlines: %d\nwords: %d\n ", totalNum[1], totalNum[0], totalNum[2]);
		
	}
	
	else{
		
		printf("Error!\n");
	}

	 
	return 0;
}



int *getCharNum(char *name, int *totalNum){
	
	
	
	FILE*fp;
	
	char buffer[10000]; 
	
	int bufferLen; 
	
	int i;  //当前读到缓冲区的第i个字符
	
	char c; //读取到的字符
	
	int isLastBlank = 0; 
	
	int charNum = 0; 
	
	int wordNum = 0; 
	
	
	printf("line	words	chars\n");

	if( (fp=fopen(name, "rb"))== NULL ){
		
		perror(name);
		
		return NULL;
	}
	
	while(fgets(buffer, 10000, fp)!= NULL){
		
		bufferLen = strlen(buffer);

		
		for( i=0; i<bufferLen; i++){
			
			c = buffer[i];
		
			if( c==' ' || c=='\t'){	
			
			//遇到空格
			!isLastBlank && wordNum++; //如果上个字符不是空格,那么单词数加1
			
			isLastBlank = 1;
			
		}else if(c!='\n'&&c!='\r'){
			
			//忽略换行符
			
			charNum++; //如果既不是换行符也不是空格，字符数加1
			
			isLastBlank = 0;
			
			}
			
		}
		!isLastBlank && wordNum++;//如果最后一个字符不是空格，那么单词数加1
		
		isLastBlank = 1; //每次换行重置为1
		

		totalNum[0]++;//总行数
		
		totalNum[1] += charNum;//总字符数
		
		totalNum[2] += wordNum;//总单词数
		
		printf("%-7d%-7d%d\n" , totalNum[0], wordNum, charNum);
		
		//置零，重新统计下一行
		charNum = 0;
		
		wordNum = 0;
	}
	 printf("\n");
		if( (fp=fopen(name, "rb"))== NULL ){
		
		perror(name);
		
		return NULL;
	}
	unsigned x,j,times=0;
			for(x=0;x<99;x++)
    {
        fscanf(fp,"%s",a[x].str);//输入字符串存储到结构体中
        
        a[x].n++; //计算出现次数   
		 
        for(j=0;j<x;j++)
        
            if(strcmp(a[x].str,a[j].str)==0) //如果和前面的相同
            
            {
                if(a[j].n!=0)a[j].n++; //不为0的加一
                
                a[x].n=0; //置为0
                
            }
            times++; //计算字符串个数
            
            if(fgetc(fp)==EOF)break;        
			   
    }
    
    for(x=0;x<times-1;x++)  //冒泡法排序
    
        for(j=0;j<times-1-x;j++)
        
            if(a[j].n<a[j+1].n) //对出现次数进行降序排序
            
            {       //交换结构体
            
                t=a[j];
                
                a[j]=a[j+1];
                
                a[j+1]=t; 
                
            }
            
            for(x=0;x<10;x++) {//输出前十个出现次数最多
            
                printf("%s  %d\n",a[x].str,a[x].n);
            }
        
             printf("\n");
	
	
		return totalNum;
}

